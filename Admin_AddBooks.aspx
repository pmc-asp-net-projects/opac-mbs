<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin_AddBooks.aspx.cs" Inherits="Admin_AddBooks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrator Page</title>
</head>
<body bgcolor="#360233">
<center>
    <form id="form1" runat="server">
    <div>
        &nbsp;<br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <span style="font-size: 16pt; color: #ffffff; font-family: Garamond"><strong>ADMINISTRATOR<br />
        </strong></span>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin.aspx">Home</asp:HyperLink><span style="font-size: 16pt; color: #ffffff"><strong>
                | </strong></span>
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddBooks.aspx">Add Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditBooks.aspx">Edit Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink4" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditReservations.aspx">Edit Reservations</asp:HyperLink><span
                style="font-size: 16pt; color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddUsers.aspx">Add Users</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"> | </span>
        <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/Default.aspx" />
        <br />
        <br />
        <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False"
            CellPadding="4" DataKeyNames="Accession" DataSourceID="SqlDataSource1" ForeColor="#333333"
            GridLines="None" Height="50px" Width="125px">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <Fields>
                <asp:BoundField DataField="Accession" HeaderText="Accession" InsertVisible="False"
                    ReadOnly="True" SortExpression="Accession" />
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                <asp:BoundField DataField="Volume" HeaderText="Volume" SortExpression="Volume" />
                <asp:BoundField DataField="EditionNumber" HeaderText="EditionNumber" SortExpression="EditionNumber" />
                <asp:BoundField DataField="AuthorLastName" HeaderText="AuthorLastName" SortExpression="AuthorLastName" />
                <asp:BoundField DataField="AuthorFirstName" HeaderText="AuthorFirstName" SortExpression="AuthorFirstName" />
                <asp:BoundField DataField="Publisher" HeaderText="Publisher" SortExpression="Publisher" />
                <asp:BoundField DataField="CopyrightYear" HeaderText="CopyrightYear" SortExpression="CopyrightYear" />
                <asp:BoundField DataField="Source" HeaderText="Source" SortExpression="Source" />
                <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
                <asp:BoundField DataField="SubjectID" HeaderText="SubjectID" SortExpression="SubjectID" />
                <asp:BoundField DataField="Author_Number" HeaderText="Author_Number" SortExpression="Author_Number" />
                <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
                <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" />
                <asp:BoundField DataField="Class_Number" HeaderText="Class_Number" SortExpression="Class_Number" />
                <asp:BoundField DataField="Classification" HeaderText="Classification" SortExpression="Classification" />
                <asp:BoundField DataField="ISBN" HeaderText="ISBN" SortExpression="ISBN" />
                <asp:BoundField DataField="Pages" HeaderText="Pages" SortExpression="Pages" />
                <asp:BoundField DataField="Physical_Description" HeaderText="Physical_Description"
                    SortExpression="Physical_Description" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Library Collection] WHERE [Accession] = @Accession"
            InsertCommand="INSERT INTO [Library Collection] ([Date], [Title], [Volume], [EditionNumber], [AuthorLastName], [AuthorFirstName], [Publisher], [CopyrightYear], [Source], [Subject], [SubjectID], [Author Number], [Cost], [Notes], [Class Number], [Classification], [ISBN], [Pages], [Physical Description]) VALUES (@Date, @Title, @Volume, @EditionNumber, @AuthorLastName, @AuthorFirstName, @Publisher, @CopyrightYear, @Source, @Subject, @SubjectID, @Author_Number, @Cost, @Notes, @Class_Number, @Classification, @ISBN, @Pages, @Physical_Description)"
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Accession], [Date], [Title], [Volume], [EditionNumber], [AuthorLastName], [AuthorFirstName], [Publisher], [CopyrightYear], [Source], [Subject], [SubjectID], [Author Number] AS Author_Number, [Cost], [Notes], [Class Number] AS Class_Number, [Classification], [ISBN], [Pages], [Physical Description] AS Physical_Description, [upsize_ts] FROM [Library Collection]"
            UpdateCommand="UPDATE [Library Collection] SET [Date] = @Date, [Title] = @Title, [Volume] = @Volume, [EditionNumber] = @EditionNumber, [AuthorLastName] = @AuthorLastName, [AuthorFirstName] = @AuthorFirstName, [Publisher] = @Publisher, [CopyrightYear] = @CopyrightYear, [Source] = @Source, [Subject] = @Subject, [SubjectID] = @SubjectID, [Author Number] = @Author_Number, [Cost] = @Cost, [Notes] = @Notes, [Class Number] = @Class_Number, [Classification] = @Classification, [ISBN] = @ISBN, [Pages] = @Pages, [Physical Description] = @Physical_Description WHERE [Accession] = @Accession">
            <InsertParameters>
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Volume" Type="String" />
                <asp:Parameter Name="EditionNumber" Type="String" />
                <asp:Parameter Name="AuthorLastName" Type="String" />
                <asp:Parameter Name="AuthorFirstName" Type="String" />
                <asp:Parameter Name="Publisher" Type="String" />
                <asp:Parameter Name="CopyrightYear" Type="Int32" />
                <asp:Parameter Name="Source" Type="String" />
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SubjectID" Type="Double" />
                <asp:Parameter Name="Author_Number" Type="String" />
                <asp:Parameter Name="Cost" Type="String" />
                <asp:Parameter Name="Notes" Type="String" />
                <asp:Parameter Name="Class_Number" Type="String" />
                <asp:Parameter Name="Classification" Type="String" />
                <asp:Parameter Name="ISBN" Type="String" />
                <asp:Parameter Name="Pages" Type="Int32" />
                <asp:Parameter Name="Physical_Description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Volume" Type="String" />
                <asp:Parameter Name="EditionNumber" Type="String" />
                <asp:Parameter Name="AuthorLastName" Type="String" />
                <asp:Parameter Name="AuthorFirstName" Type="String" />
                <asp:Parameter Name="Publisher" Type="String" />
                <asp:Parameter Name="CopyrightYear" Type="Int32" />
                <asp:Parameter Name="Source" Type="String" />
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SubjectID" Type="Double" />
                <asp:Parameter Name="Author_Number" Type="String" />
                <asp:Parameter Name="Cost" Type="String" />
                <asp:Parameter Name="Notes" Type="String" />
                <asp:Parameter Name="Class_Number" Type="String" />
                <asp:Parameter Name="Classification" Type="String" />
                <asp:Parameter Name="ISBN" Type="String" />
                <asp:Parameter Name="Pages" Type="Int32" />
                <asp:Parameter Name="Physical_Description" Type="String" />
                <asp:Parameter Name="Accession" Type="Int64" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="Accession" Type="Int64" />
            </DeleteParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
    </center>
</body>
</html>
