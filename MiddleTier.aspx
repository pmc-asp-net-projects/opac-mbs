<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MiddleTier.aspx.cs" Inherits="MiddleTier" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div>
        &nbsp; &nbsp;
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Library Collection] WHERE [Accession] = @Accession"
            InsertCommand="INSERT INTO [Library Collection] ([Date], [Title], [Volume], [EditionNumber], [AuthorLastName], [AuthorFirstName], [Publisher], [CopyrightYear], [Source], [Subject], [SubjectID], [Author Number], [Cost], [Notes], [Class Number], [Classification], [ISBN], [Pages], [Physical Description]) VALUES (@Date, @Title, @Volume, @EditionNumber, @AuthorLastName, @AuthorFirstName, @Publisher, @CopyrightYear, @Source, @Subject, @SubjectID, @Author_Number, @Cost, @Notes, @Class_Number, @Classification, @ISBN, @Pages, @Physical_Description)"
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Accession], [Date], [Title], [Volume], [EditionNumber], [AuthorLastName], [AuthorFirstName], [Publisher], [CopyrightYear], [Source], [Subject], [SubjectID], [Author Number] AS Author_Number, [Cost], [Notes], [Class Number] AS Class_Number, [Classification], [ISBN], [Pages], [Physical Description] AS Physical_Description, [upsize_ts] FROM [Library Collection]"
            UpdateCommand="UPDATE [Library Collection] SET [Date] = @Date, [Title] = @Title, [Volume] = @Volume, [EditionNumber] = @EditionNumber, [AuthorLastName] = @AuthorLastName, [AuthorFirstName] = @AuthorFirstName, [Publisher] = @Publisher, [CopyrightYear] = @CopyrightYear, [Source] = @Source, [Subject] = @Subject, [SubjectID] = @SubjectID, [Author Number] = @Author_Number, [Cost] = @Cost, [Notes] = @Notes, [Class Number] = @Class_Number, [Classification] = @Classification, [ISBN] = @ISBN, [Pages] = @Pages, [Physical Description] = @Physical_Description WHERE [Accession] = @Accession">
            <InsertParameters>
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Volume" Type="String" />
                <asp:Parameter Name="EditionNumber" Type="String" />
                <asp:Parameter Name="AuthorLastName" Type="String" />
                <asp:Parameter Name="AuthorFirstName" Type="String" />
                <asp:Parameter Name="Publisher" Type="String" />
                <asp:Parameter Name="CopyrightYear" Type="Int32" />
                <asp:Parameter Name="Source" Type="String" />
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SubjectID" Type="Double" />
                <asp:Parameter Name="Author_Number" Type="String" />
                <asp:Parameter Name="Cost" Type="String" />
                <asp:Parameter Name="Notes" Type="String" />
                <asp:Parameter Name="Class_Number" Type="String" />
                <asp:Parameter Name="Classification" Type="String" />
                <asp:Parameter Name="ISBN" Type="String" />
                <asp:Parameter Name="Pages" Type="Int32" />
                <asp:Parameter Name="Physical_Description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Volume" Type="String" />
                <asp:Parameter Name="EditionNumber" Type="String" />
                <asp:Parameter Name="AuthorLastName" Type="String" />
                <asp:Parameter Name="AuthorFirstName" Type="String" />
                <asp:Parameter Name="Publisher" Type="String" />
                <asp:Parameter Name="CopyrightYear" Type="Int32" />
                <asp:Parameter Name="Source" Type="String" />
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SubjectID" Type="Double" />
                <asp:Parameter Name="Author_Number" Type="String" />
                <asp:Parameter Name="Cost" Type="String" />
                <asp:Parameter Name="Notes" Type="String" />
                <asp:Parameter Name="Class_Number" Type="String" />
                <asp:Parameter Name="Classification" Type="String" />
                <asp:Parameter Name="ISBN" Type="String" />
                <asp:Parameter Name="Pages" Type="Int32" />
                <asp:Parameter Name="Physical_Description" Type="String" />
                <asp:Parameter Name="Accession" Type="Int64" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="Accession" Type="Int64" />
            </DeleteParameters>
        </asp:SqlDataSource>
        &nbsp;
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Reservations] WHERE [ReservationNo] = @ReservationNo"
            InsertCommand="INSERT INTO [Reservations] ([AccessionNo], [Title], [Author], [Username], [DateReserved], [DateDue]) VALUES (@AccessionNo, @Title, @Author, @Username, @DateReserved, @DateDue)"
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [ReservationNo], [AccessionNo], [Title], [Author], [Username], [DateReserved], [DateDue] FROM [Reservations]"
            UpdateCommand="UPDATE [Reservations] SET [AccessionNo] = @AccessionNo, [Title] = @Title, [Author] = @Author, [Username] = @Username, [DateReserved] = @DateReserved, [DateDue] = @DateDue WHERE [ReservationNo] = @ReservationNo">
            <InsertParameters>
                <asp:Parameter Name="AccessionNo" Type="Int64" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Author" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="DateReserved" Type="String" />
                <asp:Parameter Name="DateDue" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="AccessionNo" Type="Int64" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Author" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="DateReserved" Type="String" />
                <asp:Parameter Name="DateDue" Type="String" />
                <asp:Parameter Name="ReservationNo" Type="Int64" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="ReservationNo" Type="Int64" />
            </DeleteParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
