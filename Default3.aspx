<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="FlashControl" Namespace="Bewise.Web.UI.WebControls" TagPrefix="Bewise" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MBS OPAC</title>
<script language="javascript" type="text/javascript">
<!--

function TABLE1_onclick() {

}

// -->
</script>
</head>
<body bgcolor="#360233">
    <form id="form1" runat="server">
    <div style="text-align: center; background-color: #360233;">
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <br />
        <table>
            <tr>
                <td bgcolor="#fff8ec" style="width: 515px; text-align: center">
                    <br />
                    <br />
        <asp:Login ID="Login1" runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderPadding="4"
            BorderStyle="Solid" BorderWidth="1px"
            DestinationPageUrl="~/Admin.aspx" Font-Names="Verdana" Font-Size="0.8em"
            ForeColor="#333333">
            <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <TextBoxStyle Font-Size="0.8em" />
            <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px"
                Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
        </asp:Login>
                    <br />
                </td>
            </tr>
        </table>
        <br />
        </div>
    </form>
</body>
</html>
