<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlashMain.aspx.cs" Inherits="FlashMain" %>

<%@ Register Assembly="FlashControl" Namespace="Bewise.Web.UI.WebControls" TagPrefix="Bewise" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MBS OPAC</title>
</head>
<body bgcolor="#000000">
    <form id="form1" runat="server">
    <div style="text-align: center">
        <bewise:flashcontrol id="FlashControl1" runat="server" height="100%" movieurl="~/opac_final19.swf"
            width="100%"></bewise:flashcontrol>
        &nbsp;</div>
    </form>
</body>
</html>
