<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Help.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="FlashControl" Namespace="Bewise.Web.UI.WebControls" TagPrefix="Bewise" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MBS OPAC</title>
<script language="javascript" type="text/javascript">
<!--

function TABLE1_onclick() {

}

// -->
</script>
</head>
<body bgcolor="#360233" style="font-size: 12pt">
    <form id="form1" runat="server">
    <div style="text-align: center; background-color: #360233;">
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <br />
        <table>
            <tr>
                <td bgcolor="#fff8ec" style="width: 515px; text-align: center">
                    <center>
                        <br />
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Back To Main Menu</asp:HyperLink>&nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <span style="font-family: Garamond">Getting Started:</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <?xml namespace="" ns="urn:schemas-microsoft-com:office:office" prefix="o" ?><o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <span style="font-family: Garamond">This guide will help you how to use the MBS OPAC:</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <span style="font-family: Garamond"></span><span style="font-family: Garamond"><span
                                style="mso-list: Ignore">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                <strong>
                                &nbsp;I.<span style="font-weight: normal; line-height: normal; font-style: normal;
                                    font-variant: normal"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                </span></strong></span><u><strong>How to register?</strong></u></span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.25in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.5in; text-align: left">
                            <span style="font-family: Garamond">Note: the registration is exclusive only for the
                                student who are enrolled in Manila Bible Seminary (MBS) </span>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.5in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"><span style="mso-list: Ignore">1.<span style="font-weight: normal;
                                line-height: normal; font-style: normal; font-variant: normal"> &nbsp; &nbsp;&nbsp;
                            </span></span>See the Librarian, show your C.O.R and the librarian will register your
                                account and then you can already access the MBS OPAC.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"></span><span style="font-family: Garamond"><strong>
                                <span
                                style="mso-list: Ignore">II.<span style="font-weight: normal; line-height: normal;
                                    font-style: normal; font-variant: normal"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; </span></span><u>How to Log in?</u></strong></span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"><u></u></span><span style="font-family: Garamond">
                                By using your user name and password given by the Librarian you can now access the
                                MBS OPAC.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"><span style="mso-list: Ignore">1.<span style="font-weight: normal;
                                line-height: normal; font-style: normal; font-variant: normal"> &nbsp; &nbsp;&nbsp;
                            </span></span>Enter your user name and password</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"><span style="mso-list: Ignore">2.<span style="font-weight: normal;
                                line-height: normal; font-style: normal; font-variant: normal"> &nbsp; &nbsp;&nbsp;
                            </span></span>Click Log in.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Note: You can click �Remember me next time� so that
                                the system will remember you and you can easily access the MBS OPAC.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            &nbsp;</p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond"></span><span style="font-family: Garamond"><strong>
                                <span
                                style="mso-list: Ignore">&nbsp;III.<span style="font-weight: normal; line-height: normal;
                                    font-style: normal; font-variant: normal"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; </span></span><u>How To Search?</u></strong></span></p>
                        <p>
                            <span style="font-family: Garamond"><u>
                                <o:p></o:p>
                            </u></span>&nbsp;</p>
                    </center>
                    <center>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Search � Click Search Button to search for the books
                                you are looking for.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Note. You can choose in 3 categories in order to
                                search the books you want.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Title � title of the book that you want to search.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Author � author of the book that you are searching.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Subject � what books� category you are looking for.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <span style="font-family: Garamond">
                                <o:p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;</o:p>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>
                            <span style="mso-list: Ignore">IV.<span style="font-weight: normal; line-height: normal;
                                font-style: normal; font-variant: normal"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            </span></span><u>How to Reserve?<o:p></o:p></u></strong></span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">Note:<span style="mso-spacerun: yes">&nbsp; </span>
                                The MBS OPAC reserve only up to 3 books.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.25in; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 1in; text-indent: -0.25in; text-align: left;
                            mso-list: l0 level2 lfo1; tab-stops: list 1.0in">
                            <span style="font-family: Garamond"><span style="mso-list: Ignore">1.<span style="font-weight: normal;
                                line-height: normal; font-style: normal; font-variant: normal"> &nbsp; &nbsp;&nbsp;
                            </span></span>Click the reserve button</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: left">
                            <o:p><SPAN 
      style="FONT-FAMILY: Garamond">&nbsp;</SPAN></o:p>
                        </p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond">2. To cancel the reservation, click the cancel button.</span></p>
                        <p class="MsoNormal" style="margin: 0in 0in 0pt 0.75in; text-align: left">
                            <span style="font-family: Garamond; mso-spacerun: yes">&nbsp; </span>
                        </p>
                        <br />
                    <br />
                    </center>
                </td>
            </tr>
        </table>
        <br />
        <br />
        </div>
    </form>
</body>
</html>
