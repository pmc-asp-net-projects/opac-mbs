using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MiddleTier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int Accession;
        string Date = DateTime.Today.ToShortDateString();
        string Title;
        string Volume;
        string EditionNumber;
        string AuthorLastName;
        string AuthorFirstName;
        string Publisher;
        string CopyrightYear;
        string Source;
        string Subject;
        string SubjectID;
        string Author_Number;
        string Cost;
        string Notes;
        string Class_Number;
        string Classification;
        string ISBN;
        string Pages;
        string Physical_Description;
        string Username;
        string ReservationNo;

        if (Request["Accession"] != null)       { Accession = int.Parse(Request["Accession"]);             } else { Accession = 0; }
        if (Request["Date"] != null)            { Date = Request["Date"]; } else { Date = DateTime.Today.ToShortDateString(); }
        if (Request["Title"] != null)           { Title = "'" + Request["Title"] + "'"; } else { Title = "NULL"; }
        if (Request["Volume"] != null) { Volume = "'" + Request["Volume"] + "'"; } else { Volume = "NULL"; }
        if (Request["EditionNumber"] != null) { EditionNumber = "'" + Request["EditionNumber"] + "'"; } else { EditionNumber = "NULL"; }
        if (Request["AuthorLastName"] != null) { AuthorLastName = "'" + Request["AuthorLastName"] + "'"; } else { AuthorLastName = "NULL"; }
        if (Request["AuthorFirstName"] != null) { AuthorFirstName = "'" + Request["AuthorFirstName"] + "'"; } else { AuthorFirstName = "NULL"; }
        if (Request["Publisher"] != null) { Publisher = "'" + Request["Publisher"] + "'"; } else { Publisher = "NULL"; }
        if (Request["CopyrightYear"] != null)   { CopyrightYear = Request["CopyrightYear"];     } else { CopyrightYear = "NULL"; }
        if (Request["Source"] != null) { Source = "'" + Request["Source"] + "'"; } else { Source = "NULL"; }
        if (Request["Subject"] != null) { Subject = "'" + Request["Subject"] + "'"; } else { Subject = "NULL"; }
        if (Request["SubjectID"] != null)       { SubjectID = Request["SubjectID"];  } else { SubjectID = "NULL"; }           
        if (Request["Author_Number"] != null) { Author_Number = "'" + Request["Author_Number"] + "'"; } else { Author_Number = "NULL"; }
        if (Request["Cost"] != null)            { Cost = "'" + Request["Cost"] + "'"; } else { Cost = "''"; }
        if (Request["Notes"] != null)           { Notes = "'" + Request["Notes"] + "'"; } else { Notes = "''"; }
        if (Request["Class_Number"] != null) { Class_Number = "'" + Request["Class_Number"] + "'"; } else { Class_Number = "NULL"; }
        if (Request["Classification"] != null) { Classification = "'" + Request["Classification"] + "'"; } else { Classification = "NULL"; }
        if (Request["ISBN"] != null) { ISBN = "'" + Request["ISBN"] + "'"; } else { ISBN = "NULL"; }
        if (Request["Pages"] != null) { Pages = Request["Pages"]; } else { Pages = "NULL"; }
        if (Request["Physical_Description"] != null) { Physical_Description = "'" + Request["Physical_Description"] + "'"; } else { Physical_Description = "NULL"; }
        if (Request["Username"] != null) { Username = Request["Username"]; } else { Username = "''"; }
        if (Request["ReservationNo"] != null) { ReservationNo = Request["ReservationNo"]; } else { ReservationNo = "''"; }


        if (Request["Command"] == "SignOut")
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        if (Request["Command"] == "InsertBooks")
        {
            SqlDataSource1.InsertCommand = "INSERT INTO [Library Collection] ([Date], [Title], [Volume], [EditionNumber], [AuthorLastName], [AuthorFirstName], [Publisher], [CopyrightYear], [Source], [Subject], [SubjectID], [Author Number], [Cost], [Notes], [Class Number], [Classification], [ISBN], [Pages], [Physical Description]) VALUES (" + Date + ", " + Title + ", " + Volume + ", " + EditionNumber + ", " + AuthorLastName + ", " + AuthorFirstName + ", " + Publisher + ", " + CopyrightYear + ", " + Source + ", " + Subject + ", " + SubjectID + ", " + Author_Number + ", " + Cost + ", " + Notes + ", " + Class_Number + ", " + Classification + ", " + ISBN + ", " + Pages + ", " + Physical_Description + ")";
            SqlDataSource1.Insert();
            Response.Write("// Insert Success!");
        }

        if (Request["Command"] == "DeleteBooks")
        {
            SqlDataSource1.DeleteCommand = "DELETE FROM [Library Collection] WHERE [Accession] = " + Accession;
            SqlDataSource1.Delete();
            Response.Write("// Delete Success!");
        }

        if (Request["Command"] == "UpdateBooks")
        {
            SqlDataSource1.UpdateCommand = "UPDATE [Library Collection] SET [Date] = " + Date + ", [Title] = " + Title + ", [Volume] = " + Volume + ", [EditionNumber] = " + EditionNumber + ", [AuthorLastName] = " + AuthorLastName + ", [AuthorFirstName] = " + AuthorFirstName + ", [Publisher] = " + Publisher + ", [CopyrightYear] = " + CopyrightYear + ", [Source] = " + Source + ", [Subject] = " + Subject + ", [SubjectID] = " + SubjectID + ", [Author Number] = " + Author_Number + ", [Cost] = " + Cost + ", [Notes] = " + Notes + ", [Class Number] = " + Class_Number + ", [Classification] = " + Classification + ", [ISBN] = " + ISBN + ", [Pages] = " + Pages + ", [Physical Description] = " + Physical_Description + " WHERE [Accession] = " + Accession;
            SqlDataSource1.Update();
            Response.Write("// Update Success!");
        }

        string SearchBy;
        string SearchWord;
        string SearchOrder;
        string SearchOrderBy;
        int SearchCounter;

        if (Request["SearchBy"] != null)
        {
            SearchBy = " WHERE [" + Request["SearchBy"] + "]";
        }
        else
        {
            SearchBy = " WHERE [Title]";
        }

        if (Request["SearchWord"] != null)
        {
            SearchWord = SearchBy + " LIKE '%" + Request["SearchWord"] + "%'";
        }
        else
        {
            SearchWord = SearchBy + " = ''";
        }

        if (Request["SearchOrder"] != null)
        {
            SearchOrder = " ORDER BY " + Request["SearchOrder"];
        }
        else
        {
            SearchOrder = " ORDER BY [Accession]";
        }

        if (Request["SearchOrderBy"] != null)
        {
            SearchOrderBy = SearchOrder + " " + Request["SearchOrderBy"];
        }
        else
        {
            SearchOrderBy = SearchOrder + " ASC";
        }

        if (Request["SearchPage"] != null)
        {
            SearchCounter = int.Parse(Request["SearchPage"]) * 10;
        }
        else
        {
            SearchCounter = 0;
        }

        if (Request["Command"] == "GetBooks")
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Library Collection]" + SearchWord + SearchOrderBy;
            System.Data.DataView dv = ((System.Data.DataView)(SqlDataSource1.Select(DataSourceSelectArguments.Empty)));

            // GENERAL
            if (dv.Table.Rows.Count == 0)
            {
                Response.Write("&TotalRecords=EMPTY");
            }
            else
            {
                Response.Write("&TotalRecords=" + dv.Table.Rows.Count);
            }
            Response.Write("&TotalPages=" + dv.Table.Rows.Count / 10);

            if (ValueOrNull(dv) != null)
            {
                if (0 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 0
                    Response.Write("&Row0Accession=" + dv.Table.Rows[0 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row0Title=" + dv.Table.Rows[0 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row0Author=" + dv.Table.Rows[0 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row0Accession= ");
                    Response.Write("&Row0Title= ");
                    Response.Write("&Row0Author= ");
                }
                if (1 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 1
                    Response.Write("&Row1Accession=" + dv.Table.Rows[1 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row1Title=" + dv.Table.Rows[1 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row1Author=" + dv.Table.Rows[1 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row1Accession= ");
                    Response.Write("&Row1Title= ");
                    Response.Write("&Row1Author= ");
                }
                if (2 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 2
                    Response.Write("&Row2Accession=" + dv.Table.Rows[2 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row2Title=" + dv.Table.Rows[2 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row2Author=" + dv.Table.Rows[2 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row2Accession= ");
                    Response.Write("&Row2Title= ");
                    Response.Write("&Row2Author= ");
                }
                if (3 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 3
                    Response.Write("&Row3Accession=" + dv.Table.Rows[3 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row3Title=" + dv.Table.Rows[3 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row3Author=" + dv.Table.Rows[3 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row3Accession= ");
                    Response.Write("&Row3Title= ");
                    Response.Write("&Row3Author= ");
                }
                if (4 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 4
                    Response.Write("&Row4Accession=" + dv.Table.Rows[4 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row4Title=" + dv.Table.Rows[4 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row4Author=" + dv.Table.Rows[4 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row4Accession= ");
                    Response.Write("&Row4Title= ");
                    Response.Write("&Row4Author= ");
                }
                if (5 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 5
                    Response.Write("&Row5Accession=" + dv.Table.Rows[5 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row5Title=" + dv.Table.Rows[5 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row5Author=" + dv.Table.Rows[5 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row5Accession= ");
                    Response.Write("&Row5Title= ");
                    Response.Write("&Row5Author= ");
                }
                if (6 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 6
                    Response.Write("&Row6Accession=" + dv.Table.Rows[6 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row6Title=" + dv.Table.Rows[6 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row6Author=" + dv.Table.Rows[6 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row6Accession= ");
                    Response.Write("&Row6Title= ");
                    Response.Write("&Row6Author= ");
                }
                if (7 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 7
                    Response.Write("&Row7Accession=" + dv.Table.Rows[7 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row7Title=" + dv.Table.Rows[7 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row7Author=" + dv.Table.Rows[7 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row7Accession= ");
                    Response.Write("&Row7Title= ");
                    Response.Write("&Row7Author= ");
                }
                if (8 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 8
                    Response.Write("&Row8Accession=" + dv.Table.Rows[8 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row8Title=" + dv.Table.Rows[8 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row8Author=" + dv.Table.Rows[8 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row8Accession= ");
                    Response.Write("&Row8Title= ");
                    Response.Write("&Row8Author= ");
                }
                if (9 + SearchCounter < dv.Table.Rows.Count)
                {
                    // ROW 9
                    Response.Write("&Row9Accession=" + dv.Table.Rows[9 + SearchCounter]["Accession"].ToString());
                    Response.Write("&Row9Title=" + dv.Table.Rows[9 + SearchCounter]["Title"].ToString());
                    Response.Write("&Row9Author=" + dv.Table.Rows[9 + SearchCounter]["AuthorLastname"].ToString());
                }
                else
                {
                    Response.Write("&Row9Accession= ");
                    Response.Write("&Row9Title= ");
                    Response.Write("&Row9Author= ");
                }
            
            }
        }

        if (Request["Command"] == "GetBooksDescription")
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Library Collection] WHERE [Accession] = " + Accession;
            System.Data.DataView dv = ((System.Data.DataView)(SqlDataSource1.Select(DataSourceSelectArguments.Empty)));

            // DESCRIPTION
            Response.Write("&Accession=" + dv.Table.Rows[0]["Accession"].ToString());
            Response.Write("&Date=" + dv.Table.Rows[0]["Date"].ToString());
            Response.Write("&Title=" + dv.Table.Rows[0]["Title"].ToString());
            Response.Write("&Volume=" + dv.Table.Rows[0]["Volume"].ToString());
            Response.Write("&EditionNumber=" + dv.Table.Rows[0]["EditionNumber"].ToString());
            Response.Write("&AuthorLastName=" + dv.Table.Rows[0]["AuthorLastName"].ToString());
            Response.Write("&AuthorFirstName=" + dv.Table.Rows[0]["AuthorFirstName"].ToString());
            Response.Write("&Publisher=" + dv.Table.Rows[0]["Publisher"].ToString());
            Response.Write("&CopyrightYear=" + dv.Table.Rows[0]["CopyrightYear"].ToString());
            Response.Write("&Source=" + dv.Table.Rows[0]["Source"].ToString());
            Response.Write("&Subject=" + dv.Table.Rows[0]["Subject"].ToString());
            Response.Write("&SubjectID=" + dv.Table.Rows[0]["SubjectID"].ToString());
            Response.Write("&Author_Number=" + dv.Table.Rows[0]["Author Number"].ToString());
            Response.Write("&Cost=" + dv.Table.Rows[0]["Cost"].ToString());
            Response.Write("&Notes=" + dv.Table.Rows[0]["Notes"].ToString());
            Response.Write("&Class_Number=" + dv.Table.Rows[0]["Class Number"].ToString());
            Response.Write("&Classification=" + dv.Table.Rows[0]["Classification"].ToString());
            Response.Write("&ISBN=" + dv.Table.Rows[0]["ISBN"].ToString());
            Response.Write("&Pages=" + dv.Table.Rows[0]["Pages"].ToString());
            Response.Write("&Physical_Description=" + dv.Table.Rows[0]["Physical Description"].ToString());
        }

        if (Request["Command"] == "GetReservedBooks")
        {
            Response.Write("&Username=" + User.Identity.Name.ToString());

            SqlDataSource2.SelectCommand = "SELECT * FROM [Reservations] WHERE [Username] = '" + User.Identity.Name.ToString() + "'";
            System.Data.DataView dv = ((System.Data.DataView)(SqlDataSource2.Select(DataSourceSelectArguments.Empty)));

            if (dv.Table.Rows.Count == 0)
            {
                Response.Write("&TotalReservations=EMPTY");
            }
            else
            {
                Response.Write("&TotalReservations=" + dv.Table.Rows.Count);
            }

            // ROW 0
            if (0 < dv.Table.Rows.Count)
            {
                Response.Write("&Row0ReservationNo=" + dv.Table.Rows[0]["ReservationNo"].ToString());
                Response.Write("&Row0AccessionNo=" + dv.Table.Rows[0]["AccessionNo"].ToString());
                Response.Write("&Row0Title=" + dv.Table.Rows[0]["Title"].ToString());
                Response.Write("&Row0Author=" + dv.Table.Rows[0]["Author"].ToString());
                Response.Write("&Row0Username=" + dv.Table.Rows[0]["Username"].ToString());
                Response.Write("&Row0DateReserved=" + dv.Table.Rows[0]["DateReserved"].ToString());
                Response.Write("&Row0DateDue=" + dv.Table.Rows[0]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row0ReservationNo= ");
                Response.Write("&Row0AccessionNo= ");
                Response.Write("&Row0Title= ");
                Response.Write("&Row0Author= ");
                Response.Write("&Row0Username= ");
                Response.Write("&Row0DateReserved= ");
                Response.Write("&Row0DateDue= ");
            }

            // ROW 1
            if (1 < dv.Table.Rows.Count)
            {
                Response.Write("&Row1ReservationNo=" + dv.Table.Rows[1]["ReservationNo"].ToString());
                Response.Write("&Row1AccessionNo=" + dv.Table.Rows[1]["AccessionNo"].ToString());
                Response.Write("&Row1Title=" + dv.Table.Rows[1]["Title"].ToString());
                Response.Write("&Row1Author=" + dv.Table.Rows[1]["Author"].ToString());
                Response.Write("&Row1Username=" + dv.Table.Rows[1]["Username"].ToString());
                Response.Write("&Row1DateReserved=" + dv.Table.Rows[1]["DateReserved"].ToString());
                Response.Write("&Row1DateDue=" + dv.Table.Rows[1]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row1ReservationNo= ");
                Response.Write("&Row1AccessionNo= ");
                Response.Write("&Row1Title= ");
                Response.Write("&Row1Author= ");
                Response.Write("&Row1Username= ");
                Response.Write("&Row1DateReserved= ");
                Response.Write("&Row1DateDue= ");
            }

            // ROW 2
            if (2 < dv.Table.Rows.Count)
            {
                Response.Write("&Row2ReservationNo=" + dv.Table.Rows[2]["ReservationNo"].ToString());
                Response.Write("&Row2AccessionNo=" + dv.Table.Rows[2]["AccessionNo"].ToString());
                Response.Write("&Row2Title=" + dv.Table.Rows[2]["Title"].ToString());
                Response.Write("&Row2Author=" + dv.Table.Rows[2]["Author"].ToString());
                Response.Write("&Row2Username=" + dv.Table.Rows[2]["Username"].ToString());
                Response.Write("&Row2DateReserved=" + dv.Table.Rows[2]["DateReserved"].ToString());
                Response.Write("&Row2DateDue=" + dv.Table.Rows[2]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row2ReservationNo= ");
                Response.Write("&Row2AccessionNo= ");
                Response.Write("&Row2Title= ");
                Response.Write("&Row2Author= ");
                Response.Write("&Row2Username= ");
                Response.Write("&Row2DateReserved= ");
                Response.Write("&Row2DateDue= ");
            }
        }

        if (Request["Command"] == "InsertReservedBooks")
        {
            if (TotalRecordsChecker(SqlDataSource2, "SELECT COUNT(*) AS [Total] FROM [Reservations] WHERE ([Username] = '" + User.Identity.Name.ToString() + "') AND ([AccessionNo] = " + Accession + ")") == 0)
            {
                Response.Write("&ServerResponse=Ok");
                
                SqlDataSource2.InsertCommand = "INSERT INTO [Reservations] ([AccessionNo], [Title], [Author], [Username], [DateReserved], [DateDue]) VALUES (" + Accession + ", " + Title + ", " + AuthorLastName + ", '" + Username + "', '" + DateTime.Today.ToShortDateString() + "', '" + DateTime.Today.AddDays(3).ToShortDateString() + "')";
                SqlDataSource2.Insert();

                Response.Write("&Username=" + User.Identity.Name.ToString());

                SqlDataSource2.SelectCommand = "SELECT * FROM [Reservations] WHERE [Username] = '" + User.Identity.Name.ToString() + "'";
                System.Data.DataView dv = ((System.Data.DataView)(SqlDataSource2.Select(DataSourceSelectArguments.Empty)));
                Response.Write("&TotalReservations=" + dv.Table.Rows.Count);

                // ROW 0
                if (0 < dv.Table.Rows.Count)
                {
                    Response.Write("&Row0ReservationNo=" + dv.Table.Rows[0]["ReservationNo"].ToString());
                    Response.Write("&Row0AccessionNo=" + dv.Table.Rows[0]["AccessionNo"].ToString());
                    Response.Write("&Row0Title=" + dv.Table.Rows[0]["Title"].ToString());
                    Response.Write("&Row0Author=" + dv.Table.Rows[0]["Author"].ToString());
                    Response.Write("&Row0Username=" + dv.Table.Rows[0]["Username"].ToString());
                    Response.Write("&Row0DateReserved=" + dv.Table.Rows[0]["DateReserved"].ToString());
                    Response.Write("&Row0DateDue=" + dv.Table.Rows[0]["DateDue"].ToString());
                }
                else
                {
                    Response.Write("&Row0ReservationNo= ");
                    Response.Write("&Row0AccessionNo= ");
                    Response.Write("&Row0Title= ");
                    Response.Write("&Row0Author= ");
                    Response.Write("&Row0Username= ");
                    Response.Write("&Row0DateReserved= ");
                    Response.Write("&Row0DateDue= ");
                }

                // ROW 1
                if (1 < dv.Table.Rows.Count)
                {
                    Response.Write("&Row1ReservationNo=" + dv.Table.Rows[1]["ReservationNo"].ToString());
                    Response.Write("&Row1AccessionNo=" + dv.Table.Rows[1]["AccessionNo"].ToString());
                    Response.Write("&Row1Title=" + dv.Table.Rows[1]["Title"].ToString());
                    Response.Write("&Row1Author=" + dv.Table.Rows[1]["Author"].ToString());
                    Response.Write("&Row1Username=" + dv.Table.Rows[1]["Username"].ToString());
                    Response.Write("&Row1DateReserved=" + dv.Table.Rows[1]["DateReserved"].ToString());
                    Response.Write("&Row1DateDue=" + dv.Table.Rows[1]["DateDue"].ToString());
                }
                else
                {
                    Response.Write("&Row1ReservationNo= ");
                    Response.Write("&Row1AccessionNo= ");
                    Response.Write("&Row1Title= ");
                    Response.Write("&Row1Author= ");
                    Response.Write("&Row1Username= ");
                    Response.Write("&Row1DateReserved= ");
                    Response.Write("&Row1DateDue= ");
                }

                // ROW 2
                if (2 < dv.Table.Rows.Count)
                {
                    Response.Write("&Row2ReservationNo=" + dv.Table.Rows[2]["ReservationNo"].ToString());
                    Response.Write("&Row2AccessionNo=" + dv.Table.Rows[2]["AccessionNo"].ToString());
                    Response.Write("&Row2Title=" + dv.Table.Rows[2]["Title"].ToString());
                    Response.Write("&Row2Author=" + dv.Table.Rows[2]["Author"].ToString());
                    Response.Write("&Row2Username=" + dv.Table.Rows[2]["Username"].ToString());
                    Response.Write("&Row2DateReserved=" + dv.Table.Rows[2]["DateReserved"].ToString());
                    Response.Write("&Row2DateDue=" + dv.Table.Rows[2]["DateDue"].ToString());
                }
                else
                {
                    Response.Write("&Row2ReservationNo= ");
                    Response.Write("&Row2AccessionNo= ");
                    Response.Write("&Row2Title= ");
                    Response.Write("&Row2Author= ");
                    Response.Write("&Row2Username= ");
                    Response.Write("&Row2DateReserved= ");
                    Response.Write("&Row2DateDue= ");
                }
            }
            else
            {
                Response.Write("&ServerResponse=Duplicate");
            }
            
        }

        if (Request["Command"] == "DeleteReservedBooks")
        {
            SqlDataSource2.DeleteCommand = "DELETE FROM [Reservations] WHERE [ReservationNo] = " + ReservationNo;
            SqlDataSource2.Delete();
            Response.Write("// Delete Success!");

            Response.Write("&Username=" + User.Identity.Name.ToString());

            SqlDataSource2.SelectCommand = "SELECT * FROM [Reservations] WHERE [Username] = '" + User.Identity.Name.ToString() + "'";
            System.Data.DataView dv = ((System.Data.DataView)(SqlDataSource2.Select(DataSourceSelectArguments.Empty)));

            Response.Write("&TotalReservations=" + dv.Table.Rows.Count);

            // ROW 0
            if (0 < dv.Table.Rows.Count)
            {
                Response.Write("&Row0ReservationNo=" + dv.Table.Rows[0]["ReservationNo"].ToString());
                Response.Write("&Row0AccessionNo=" + dv.Table.Rows[0]["AccessionNo"].ToString());
                Response.Write("&Row0Title=" + dv.Table.Rows[0]["Title"].ToString());
                Response.Write("&Row0Author=" + dv.Table.Rows[0]["Author"].ToString());
                Response.Write("&Row0Username=" + dv.Table.Rows[0]["Username"].ToString());
                Response.Write("&Row0DateReserved=" + dv.Table.Rows[0]["DateReserved"].ToString());
                Response.Write("&Row0DateDue=" + dv.Table.Rows[0]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row0ReservationNo= ");
                Response.Write("&Row0AccessionNo= ");
                Response.Write("&Row0Title= ");
                Response.Write("&Row0Author= ");
                Response.Write("&Row0Username= ");
                Response.Write("&Row0DateReserved= ");
                Response.Write("&Row0DateDue= ");
            }

            // ROW 1
            if (1 < dv.Table.Rows.Count)
            {
                Response.Write("&Row1ReservationNo=" + dv.Table.Rows[1]["ReservationNo"].ToString());
                Response.Write("&Row1AccessionNo=" + dv.Table.Rows[1]["AccessionNo"].ToString());
                Response.Write("&Row1Title=" + dv.Table.Rows[1]["Title"].ToString());
                Response.Write("&Row1Author=" + dv.Table.Rows[1]["Author"].ToString());
                Response.Write("&Row1Username=" + dv.Table.Rows[1]["Username"].ToString());
                Response.Write("&Row1DateReserved=" + dv.Table.Rows[1]["DateReserved"].ToString());
                Response.Write("&Row1DateDue=" + dv.Table.Rows[1]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row1ReservationNo= ");
                Response.Write("&Row1AccessionNo= ");
                Response.Write("&Row1Title= ");
                Response.Write("&Row1Author= ");
                Response.Write("&Row1Username= ");
                Response.Write("&Row1DateReserved= ");
                Response.Write("&Row1DateDue= ");
            }

            // ROW 2
            if (2 < dv.Table.Rows.Count)
            {
                Response.Write("&Row2ReservationNo=" + dv.Table.Rows[2]["ReservationNo"].ToString());
                Response.Write("&Row2AccessionNo=" + dv.Table.Rows[2]["AccessionNo"].ToString());
                Response.Write("&Row2Title=" + dv.Table.Rows[2]["Title"].ToString());
                Response.Write("&Row2Author=" + dv.Table.Rows[2]["Author"].ToString());
                Response.Write("&Row2Username=" + dv.Table.Rows[2]["Username"].ToString());
                Response.Write("&Row2DateReserved=" + dv.Table.Rows[2]["DateReserved"].ToString());
                Response.Write("&Row2DateDue=" + dv.Table.Rows[2]["DateDue"].ToString());
            }
            else
            {
                Response.Write("&Row2ReservationNo= ");
                Response.Write("&Row2AccessionNo= ");
                Response.Write("&Row2Title= ");
                Response.Write("&Row2Author= ");
                Response.Write("&Row2Username= ");
                Response.Write("&Row2DateReserved= ");
                Response.Write("&Row2DateDue= ");
            }
        }

    }

    public static object ValueOrNull(object value)
    {
        // SPECIAL THANKS: Rex Lin, http://forums.asp.net/thread/1606277.aspx
        if (value == DBNull.Value)
        {
            return null;
        }
        else
        {
            return value;
        }
    }

    public static int TotalRecordsChecker(SqlDataSource datasource, string SearchString03)
    {
        // SPECIAL THANKS: limno, http://forums.asp.net/thread/1272026.aspx
        // March 6, 2007: Made a own method ^_^
        // April 5, 2007: It now only needs is datasource ^_^

        datasource.SelectCommand = SearchString03;
        System.Data.DataView dv = ((System.Data.DataView)(datasource.Select(DataSourceSelectArguments.Empty)));

        if (ValueOrNull(dv) != null) { return (Int32.Parse(dv.Table.Rows[0]["Total"].ToString())); }
        else { return (0); }
    }
}
