<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin_AddUsers.aspx.cs" Inherits="Admin_AddUsers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrator Page</title>
</head>
<body bgcolor="#360233">
<center>
    <form id="form1" runat="server">
    <div>
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <span style="font-size: 16pt; color: #ffffff; font-family: Garamond"><strong>ADMINISTRATOR<br />
        </strong></span>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin.aspx">Home</asp:HyperLink><span style="font-size: 16pt; color: #ffffff"><strong>
                | </strong></span>
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddBooks.aspx">Add Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditBooks.aspx">Edit Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink4" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditReservations.aspx">Edit Reservations</asp:HyperLink><span
                style="font-size: 16pt; color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddUsers.aspx">Add Users</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"> | </span>
        <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/Default.aspx" />
        <br />
        <br />
        <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8"
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em">
            <WizardSteps>
                <asp:CreateUserWizardStep runat="server">
                </asp:CreateUserWizardStep>
                <asp:CompleteWizardStep runat="server">
                </asp:CompleteWizardStep>
            </WizardSteps>
            <SideBarStyle BackColor="#5D7B9D" BorderWidth="0px" Font-Size="0.9em" VerticalAlign="Top" />
            <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <SideBarButtonStyle BorderWidth="0px" Font-Names="Verdana" ForeColor="White" />
            <NavigationButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
                BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
            <HeaderStyle BackColor="#5D7B9D" BorderStyle="Solid" Font-Bold="True" Font-Size="0.9em"
                ForeColor="White" HorizontalAlign="Center" />
            <CreateUserButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
                BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
            <ContinueButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
                BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
            <StepStyle BorderWidth="0px" />
        </asp:CreateUserWizard>
        &nbsp;</div>
    </form>
    </center>
</body>
</html>
