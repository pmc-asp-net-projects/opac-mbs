<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="LoggedIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrator Page</title>
</head>
<body>
<body bgcolor="#360233">
<center>
    <form id="form1" runat="server">
    <div>
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <span style="font-size: 16pt; color: #ffffff; font-family: Garamond"><strong>ADMINISTRATOR<br />
        </strong></span>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin.aspx">Home</asp:HyperLink><span style="font-size: 16pt; color: #ffffff"><strong>
                | </strong></span>
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddBooks.aspx">Add Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditBooks.aspx">Edit Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink4" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditReservations.aspx">Edit Reservations</asp:HyperLink><span
                style="font-size: 16pt; color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddUsers.aspx">Add Users</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"> | </span>
        <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/Default.aspx" />
        <br />
        <br />
        &nbsp;&nbsp;<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
        <br />
        <span style="font-size: 32pt; color: #ffffff; font-family: Garamond"><strong>WELCOME
            TO THE<br />
            ADMINISTRATOR PAGE!</strong></span></div>
    </form>
    </center>
</body>
</html>
