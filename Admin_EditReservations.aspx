<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin_EditReservations.aspx.cs" Inherits="Admin_EditReservations" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrator Page</title>
</head>
<body bgcolor="#360233">
<center>
    <form id="form1" runat="server">
    <div>
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MBSBanner.JPG" Width="85%" /><br />
        <br />
        <br />
        <span style="font-size: 16pt; color: #ffffff; font-family: Garamond"><strong>ADMINISTRATOR<br />
        </strong></span>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin.aspx">Home</asp:HyperLink><span style="font-size: 16pt; color: #ffffff"><strong>
                | </strong></span>
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddBooks.aspx">Add Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditBooks.aspx">Edit Books</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink4" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_EditReservations.aspx">Edit Reservations</asp:HyperLink><span
                style="font-size: 16pt; color: #ffffff"><strong> | </strong></span>
        <asp:HyperLink ID="HyperLink5" runat="server" Font-Names="Garamond" ForeColor="#FFFFFF"
            NavigateUrl="~/Admin_AddUsers.aspx">Add Users</asp:HyperLink><span style="font-size: 16pt;
                color: #ffffff"> | </span>
        <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="Redirect" LogoutPageUrl="~/Default.aspx" />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ReservationNo" DataSourceID="SqlDataSource1"
            EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="None">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ReservationNo" HeaderText="ReservationNo" ReadOnly="True"
                    SortExpression="ReservationNo" />
                <asp:BoundField DataField="AccessionNo" HeaderText="AccessionNo" SortExpression="AccessionNo" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                <asp:BoundField DataField="Author" HeaderText="Author" SortExpression="Author" />
                <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
                <asp:BoundField DataField="DateReserved" HeaderText="DateReserved" SortExpression="DateReserved" />
                <asp:BoundField DataField="DateDue" HeaderText="DateDue" SortExpression="DateDue" />
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Reservations] WHERE [ReservationNo] = @ReservationNo"
            InsertCommand="INSERT INTO [Reservations] ([AccessionNo], [Title], [Author], [Username], [DateReserved], [DateDue]) VALUES (@AccessionNo, @Title, @Author, @Username, @DateReserved, @DateDue)"
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [ReservationNo], [AccessionNo], [Title], [Author], [Username], [DateReserved], [DateDue] FROM [Reservations]"
            UpdateCommand="UPDATE [Reservations] SET [AccessionNo] = @AccessionNo, [Title] = @Title, [Author] = @Author, [Username] = @Username, [DateReserved] = @DateReserved, [DateDue] = @DateDue WHERE [ReservationNo] = @ReservationNo">
            <InsertParameters>
                <asp:Parameter Name="AccessionNo" Type="Int64" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Author" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="DateReserved" Type="String" />
                <asp:Parameter Name="DateDue" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="AccessionNo" Type="Int64" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Author" Type="String" />
                <asp:Parameter Name="Username" Type="String" />
                <asp:Parameter Name="DateReserved" Type="String" />
                <asp:Parameter Name="DateDue" Type="String" />
                <asp:Parameter Name="ReservationNo" Type="Int64" />
            </UpdateParameters>
            <DeleteParameters>
                <asp:Parameter Name="ReservationNo" Type="Int64" />
            </DeleteParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
    </center>
</body>
</html>
